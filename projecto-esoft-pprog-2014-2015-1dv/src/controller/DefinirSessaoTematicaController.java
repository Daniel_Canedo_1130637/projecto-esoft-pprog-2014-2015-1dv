/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Empresa;
import domain.Evento;
import domain.RegistoEvento;
import domain.RegistoSessaoTematica;
import domain.SessaoTematica;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class DefinirSessaoTematicaController {

	private Empresa empresa;
	private Evento evento;
	private SessaoTematica sessaoTematica;
	private RegistoSessaoTematica rst;
	private RegistoEvento re;

	public DefinirSessaoTematicaController() {
	}

	public DefinirSessaoTematicaController(Empresa empresa, Evento evento,
										   SessaoTematica sessaoTematica) {
		this.empresa = empresa;
		this.evento = evento;
		this.sessaoTematica = sessaoTematica;
	}

	public void novaSessaoTematica() {
		rst.novaSessaoTematica();
	}

	public List<Evento> getListaEventos() {
		this.re = this.empresa.getRegistoEvento();
		return re.getListaEventos();
	}

	public void setCodigo(int codigo) {
		sessaoTematica.setCodigo(codigo);
	}

	public void setDescricao(String descricao) {
		sessaoTematica.setDescricao(descricao);
	}

	public void setListaProponentes(List listaProponentes) {
		sessaoTematica.setListaProponentes(null);
	}

	public void setEvento(Evento e) {
		e.novaSessaoTematica();
	}

	public void add(SessaoTematica st) {

	}
}
