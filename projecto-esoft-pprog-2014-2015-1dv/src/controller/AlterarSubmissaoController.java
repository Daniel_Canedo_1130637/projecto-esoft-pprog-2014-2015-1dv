/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Artigo;
import domain.Autor;
import domain.Empresa;
import domain.RegistoEvento;
import domain.RegistoUtilizador;
import domain.Submissao;
import domain.Submissivel;
import java.util.ArrayList;
import java.util.List;


public class AlterarSubmissaoController {
    
    private Empresa empresa;
    private RegistoUtilizador ru;
    private Submissivel s;
    private Submissao sub;
    private Artigo aClone;
    private Autor aut;
    
    public AlterarSubmissaoController(Empresa empresa)
    {
        this.empresa = empresa;
    }
    
    public List<Submissivel> getListaSubmissiveis()
    {
        RegistoEvento re = this.empresa.getRegistoEvento();
        List<Submissivel> list = re.getListaSubmissiveis();
        return list;
    }
    
    public List<Submissao> setSubmissivel(Submissivel s)
    {
        this.s = s;
        return this.s.getListaSubmissoes();
    }
    
    public void setSubmissao(Submissao sub)
    {
        this.sub=sub;
        Artigo a = this.sub.getArtigo();
        this.aClone = new Artigo(a);
    }
    
    public String getTitulo()
    {
        return this.aClone.getTitulo();
    }
    
    public String getResumo()
    {
        return this.aClone.getResumo();
    }
    
    public void alterarDados(String titulo, String resumo)
    {
        this.aClone.setTitulo(titulo);
        this.aClone.setResumo(resumo);
    }
    
    public List<Autor> getListaAutores()
    {
        return this.aClone.getListaAutores();
    }
    
    public void setAutor(Autor aut)
    {
        this.aut = aut;
        this.aClone.removerAutor(this.aut);
    }

    public void novoAutor(String nome, String afiliacao, String email) {
        this.aut = this.aClone.novoAutor(nome, afiliacao, email);
    }

    public void addAutor() {
        this.aClone.addAutor(this.aut);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.aClone.getPossiveisAutoresCorrespondentes();
    }

    public void setCorrespondente(Autor autor) {
        this.ru = this.empresa.getRegistoUtilizador();
        
        if(this.ru.validaAutorCorrespondente(autor))
        {
            this.aClone.setCorrespondente(autor);
        }
    }

    public void setFicheiro(String fx) {
        this.aClone.setFicheiro(fx);
    }

    public void alterarArtigo() {
        this.sub.setArtigo(this.aClone);
    }
    
    
}
