/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.CP;
import domain.CPDefinivel;
import domain.Empresa;
import domain.Evento;
import domain.RegistoEvento;
import domain.RegistoUtilizador;
import domain.SessaoTematica;
import domain.Utilizador;
import java.util.ArrayList;
import java.util.List;


public class DefinirCPSessaoTematicaController {
    
    private Empresa empresa;
    private RegistoUtilizador ru;
    private RegistoEvento re;
    private Utilizador utilizador;
    private CPDefinivel cpDefinivel;
    private CP cp;
    
    
    public DefinirCPSessaoTematicaController()
    {
        
    }
    
    public DefinirCPSessaoTematicaController(Empresa empresa)
    {
        this.empresa=empresa;
    }
    
    public List<CPDefinivel> getCPDefinivelEmDefinicaoDoUtilizador(String id) {
        this.ru = this.empresa.getRegistoUtilizador();
        this.re.getListaEventosOrganizadorSemCP(id);
        List<CPDefinivel> list = new ArrayList<>();
        list = this.re.getCPDefinivelEmDefinicaoDoUtilizador(id);
        return list;
    }
    
    public void novaCP(CPDefinivel cpDef)
    {
        this.cpDefinivel = cpDef;
        this.cp=this.cpDefinivel.novaCP();
    }

    public void novoMembroCP(String id)
    {
        this.utilizador = this.ru.getUtilizador(id);
        this.cp.addMembroCP(utilizador);
    }

    public void addMembroCP() {
        this.cp.addMembroCP();
    }


    public void setCP() {
        if(this.cpDefinivel instanceof Evento)
        {
            Evento e = new Evento(this.cpDefinivel);
            e.setCP(cp);
        }
        else
        {
            SessaoTematica st = new SessaoTematica(this.cpDefinivel);
            st.setCP(cp);
        }
    }
}
