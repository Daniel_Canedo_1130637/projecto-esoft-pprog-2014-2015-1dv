/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Artigo;
import domain.Autor;
import domain.Empresa;
import domain.RegistoEvento;
import domain.RegistoUtilizador;
import domain.Submissao;
import domain.Submissivel;
import java.util.ArrayList;
import java.util.List;


public class SubmissaoArtigoController {
    
    private Empresa empresa;
    private Submissivel s;
    private Artigo a;
    private Autor aut;
    private Submissao sub;
    private RegistoUtilizador ru;
    
    public SubmissaoArtigoController()
    {
        
    }
    
    public SubmissaoArtigoController(Empresa empresa)
    {
        this.empresa = empresa;
    }
    
    public List<Submissivel> getListaSubmissiveis()
    {
        RegistoEvento re = this.empresa.getRegistoEvento();
        List<Submissivel> list = re.getListaSubmissiveis();
        return list;
    }
    
    
    public void novaSubmissaoEm(Submissivel s)
    {
        this.s = s;
        this.sub = this.s.novaSubmissao();
        this.a = this.sub.novoArtigo();
    }
    
    public void setDados(String titulo, String resumo)
    {
        this.a.setTitulo(titulo);
        this.a.setResumo(resumo);
    }
    
    public void novoAutor(String nome, String afiliacao, String email)
    {
        this.aut = this.a.novoAutor(nome,afiliacao,email);
    }
    
    public void addAutor()
    {
        this.a.addAutor(this.aut);
    }
    
    public ArrayList<Autor> getPossiveisAutoresCorrespondentes()
    {
        return this.a.getPossiveisAutoresCorrespondentes();
    }
    
    public void setCorrespondente(Autor autor)
    {
        this.ru = this.empresa.getRegistoUtilizador();
        if(this.ru.validaAutorCorrespondente(autor))
        {
            this.a.setCorrespondente(autor);
        }
    }
    
    public void setFicheiro(String fx)
    {
        this.a.setFicheiro(fx);
    }
    
    public void registaSubmissao()
    {
        this.sub.setArtigo(this.a);
        this.s.addSubmissao(sub);
    }
    
}
