/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Empresa;
import domain.Evento;
import domain.Local;
import domain.RegistoEvento;
import domain.RegistoUtilizador;

/**
 *
 * @author João Cabral
 */
public class CriarEventoController {

	private Empresa empresa;
	private Evento evento;
	private Local local;
	private String strEvento;
	private RegistoEvento re;
	private RegistoUtilizador ru;

	public CriarEventoController() {
	}

	public CriarEventoController(Empresa empresa, Evento evento) {
		this.empresa = empresa;
		this.evento = evento;
	}

	public void novoEvento() {
		this.re = this.empresa.getRegistoEvento();
		this.evento = this.re.novoEvento();
	}

	public void setTitulo(String titulo) {
		evento.setTitulo(titulo);
	}

	public void setDescricao(String descricao) {
		evento.setDescricao(descricao);
	}

	public void setDataInicio(String dataInicio) {
		evento.setDataInicio(dataInicio);
	}

	public void setDataFim(String dataFim) {
		evento.setDataFim(dataFim);
	}

	public void setLocal(Local local) {
		evento.setLocal(local);
	}

	public void addOrganizador(String id) {
		this.ru = this.empresa.getRegistoUtilizador();
		evento.addOrganizador(id, this.ru.getUtilizador(id));
	}

	public String getEventoString() {
		strEvento = evento.toString();
		return strEvento;
	}

	public boolean registaEvento() {
		return this.re.registaEvento(evento);
	}
}
