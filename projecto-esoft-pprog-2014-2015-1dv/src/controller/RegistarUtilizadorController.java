/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Empresa;
import domain.RegistoUtilizador;
import domain.Utilizador;

/**
 *
 * @author luis
 */
public class RegistarUtilizadorController {

	private Empresa empresa;
        private RegistoUtilizador ru;
	private Utilizador utilizador;

	public RegistarUtilizadorController() {
	}

	public RegistarUtilizadorController(Empresa empresa) {
		this.empresa = empresa;
	}

	private void novoUtilizador() {
            this.ru = this.empresa.getRegistoUtilizador();
            this.ru.novoUtilizador();

	}

	private void setDados(String username, String password, String nome,
						  String email) {
            this.utilizador.setEmail(email);
            this.utilizador.setUsername(username);
            this.utilizador.setPassword(password);
            this.utilizador.setNome(nome);
            
            this.ru.registaUtilizador(this.utilizador);
	}
}
