/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import STState.STEstado;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class SessaoTematica implements Submissivel, CPDefinivel {

    private int codigo;
    private String descricao;
    private List listaProponentes;
    private ArrayList<Submissao> lsst;
    private CPSessaoTematica cpst;
    private STEstado state;

    public SessaoTematica() {
        this.lsst = new ArrayList<>();
    }

    public SessaoTematica(CPDefinivel cpDefinivel) {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List getListaProponentes() {
        return listaProponentes;
    }

    public void setListaProponentes(List listaProponentes) {
        this.listaProponentes = listaProponentes;
    }

    public STEstado getEstado() {
        return this.state;
    }

    @Override
    public SubmissaoSessaoTematica novaSubmissao() {
        SubmissaoSessaoTematica aux = new SubmissaoSessaoTematica();
        return aux;
    }

    @Override
    public void addSubmissao(Submissao sst) {
        this.lsst.add(sst);
    }

    @Override
    public CPSessaoTematica novaCP() {
        return this.cpst;
    }

    @Override
    public void setCP(CP cpst) {
        this.cpst = new CPSessaoTematica(cpst);
        this.state.setCPDefinida();
    }

    @Override
    public List<Submissao> getListaSubmissoes() {
        return this.lsst;
    }

    public void setState(String state) {
    }

    public boolean hasProponente(String id) {
        return true;
    }
}
