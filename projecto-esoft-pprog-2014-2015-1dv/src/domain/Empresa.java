/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luis
 */
public class Empresa {

	private RegistoUtilizador ru;
	private RegistoEvento re;
	private List<Submissivel> lsub;
	private List<TipoConflitoInteresse> listaTCI;

	public Empresa() {
		this.lsub = new ArrayList<>();
	}

	public Empresa(RegistoUtilizador ru, RegistoEvento re,
				   ArrayList<Submissivel> lsub) {
		this.ru = ru;
		this.re = re;
		this.lsub = lsub;
	}

	public RegistoUtilizador getRegistoUtilizador() {
		return this.ru;
	}

	public RegistoEvento getRegistoEvento() {
		return this.re;
	}

	public List<Submissivel> getListaSubmissiveis() {
		return this.lsub;
	}

	@Override
	public String toString() {
		return super.toString(); //To change body of generated methods, choose Tools | Templates.
	}

	public void setDadosTipoConflito(String dados) {
		TipoConflitoInteresse tipoCI = new TipoConflitoInteresse();
		tipoCI.setDados(dados);
		validaAdicionaTipoConflito(tipoCI);
	}

	private boolean validaAdicionaTipoConflito(TipoConflitoInteresse tipoCI) {
		listaTCI.add(tipoCI);
		return true;
	}

}
