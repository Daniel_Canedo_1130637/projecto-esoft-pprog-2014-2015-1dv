/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.List;

public interface Submissivel {
    
    public Submissao novaSubmissao();
    
    public void addSubmissao(Submissao sub);

    public List<Submissao> getListaSubmissoes();
}
