/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;



/**
 *
 * @author João Cabral
 */
public class RegistoSessaoTematica {

	private SessaoTematica st;
	private ListaSessaoTematica lst;

	public RegistoSessaoTematica() {
	}

	public void add(SessaoTematica st) {
		lst.add(st);
	}

	public void novaSessaoTematica() {
		st = new SessaoTematica();
	}

	public ListaSessaoTematica getListaSessoesTematicas() {
		return this.lst;
	}
}
