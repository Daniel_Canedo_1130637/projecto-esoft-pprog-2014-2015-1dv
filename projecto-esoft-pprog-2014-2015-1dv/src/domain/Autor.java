/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author luis
 */
public class Autor {

	private Utilizador utilizador;
	private String nome;
	private String afiliacao;
	private String email;

	public Autor() {
	}
        
        public Autor(String nome, String afiliacao, String email)
        {
            this.nome=nome;
            this.afiliacao=afiliacao;
            this.email=email;
        }

	public Autor(Utilizador m_Utilizador, String m_strNome,
				 String m_strAfiliacao, String m_strEMail) {
		this.utilizador = m_Utilizador;
		this.nome = m_strNome;
		this.afiliacao = m_strAfiliacao;
		this.email = m_strEMail;
	}
        
        public String getNome()
        {
            return this.nome;
        }
        
        public void setNome(String nome)
        {
            this.nome=nome;
        }
        
        public void setAfiliacao(String afiliacao)
        {
            this.afiliacao=afiliacao;
        }
        
        public void setEmail(String email)
        {
            this.email=email;
        }
        
        public boolean valida()
        {
            return true;
        }

	@Override
	public String toString() {
		return super.toString(); //To change body of generated methods, choose Tools | Templates.
	}

}
