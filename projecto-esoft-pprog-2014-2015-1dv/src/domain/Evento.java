/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import EventoState.EventoEstado;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luis
 */
public class Evento implements Submissivel, CPDefinivel {

	private String titulo;
	private String descricao;
	private String dataInicio;
	private String dataFim;
	private CPEvento cpe;
	private ArrayList<Submissao> lse;
	private Local local;
	private List listaOrganizadores;
	private RegistoSessaoTematica rst;
	private EventoEstado state;

	public Evento() {
	}

	public Evento(String titulo, String descricao, String dataInicio,
				  String dataFim, CPEvento cpe, Local local) {
		this.titulo = titulo;
		this.descricao = descricao;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.cpe = cpe;
		this.local = local;
		this.lse = new ArrayList<>();
	}
        
        public Evento(CPDefinivel cpDef)
        {
            
        }

        @Override
	public CPEvento novaCP() {
		return new CPEvento();
	}

	public ProcessoDistribuicao novoProcessoDistribuicao(MecanismoDistribuicao m) {
		return null;
	}

	public void setProcessoDistribuicao(ProcessoDistribuicao pd) {

	}

	public CPEvento getCp() {
		return cpe;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDataFim() {
		return dataFim;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getTitulo() {
		return titulo;
	}
        
        public EventoEstado getEstado()
        {
            return this.state;
        }

        @Override
	public void setCP(CP cpe) {
            this.cpe = new CPEvento(cpe);
            this.state.setCPDefinida();
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public List getListaOrganizadores() {
		return this.listaOrganizadores;
	}

	public void addOrganizador(String id, Utilizador u) {
		Organizador o = new Organizador(id, u);
		addOrganizador(o);
	}

	public void addOrganizador(Organizador o) {
		listaOrganizadores.add(o);
	}

	@Override
	public SubmissaoEvento novaSubmissao() {
		SubmissaoEvento aux = new SubmissaoEvento();
		return aux;
	}

	@Override
	public void addSubmissao(Submissao se) {
		this.lse.add(se);
	}

	public boolean valida() {
		return false;
	}

	public RegistoSessaoTematica getRegistoSessaoTematica() {
		return this.rst;
	}

	public void setRegistado() {
	}

	public void setState(String state) {
	}

	@Override
	public List<Submissao> getListaSubmissoes() {
		return this.lse;
	}

	@Override
	public String toString() {
		return super.toString(); //To change body of generated methods, choose Tools | Templates.
	}

	public SessaoTematica novaSessaoTematica() {
		return new SessaoTematica();
	}
        
        public boolean hasOrganizador(String id)
        {
            return true;
        }
}
