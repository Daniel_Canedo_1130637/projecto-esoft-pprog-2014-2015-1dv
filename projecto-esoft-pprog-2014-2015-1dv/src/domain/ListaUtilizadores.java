/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.List;

public class ListaUtilizadores {

	private List<Utilizador> lu;

	public ListaUtilizadores() {
		lu = new ArrayList<>();
	}

	public boolean removeUtilizador(Utilizador u) {
		return lu.remove(u);
	}

	public boolean adicionaUtilizador(Utilizador u) {
		return lu.add(u);
	}

	public Utilizador getUtilizador(String id) {
		return null;
	}

	public boolean valida() {
		return true;
	}
}
