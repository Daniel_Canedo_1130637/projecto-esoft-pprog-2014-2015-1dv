/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class RegistoEvento {

    private ListaEvento le;

    public RegistoEvento() {
        le = new ListaEvento();
    }

    public Evento novoEvento() {
        return new Evento();
    }

    public boolean registaEvento(Evento e) {
        if (this.validaEvento(e)) {
            return adicionaEvento(e);
        }
        return false;
    }

    private boolean validaEvento(Evento e) {
        return true;
    }

    private boolean adicionaEvento(Evento e) {
        return le.adicionaEvento(e);
    }

    public List<Evento> getListaEventosOrganizadorSemCP(String id) {
        return this.le.getListaEventoOrganizadorSemCP(id);
    }

    public List<Evento> getListaEventos() {
        return this.le.getListaEventos();
    }

    public List<Evento> getListaEventosRevisor(Revisor revisorID) {
        return null;
    }

    public List<Evento> getListaEventosOrganizador(String organizadorID) {
        return null;
    }

    public List<CPDefinivel> getCPDefinivelEmDefinicaoDoUtilizador(String id) {
        List<CPDefinivel> list = new ArrayList<>();
        List<Evento> listE = this.le.getListaEventos();
        for (Evento e : listE) {
            if (e.getEstado().isInSTDefinida() && e.hasOrganizador(id)) {
                list.add(e);
                RegistoSessaoTematica rst = e.getRegistoSessaoTematica();
                ListaSessaoTematica listST = rst.getListaSessoesTematicas();
                List<SessaoTematica> listST2 = listST.getCPDefiniveisEmDefinicaoDoUtilizador(id);
                for (SessaoTematica st : listST2) {
                    list.add(st);
                }
            }
        }
        return list;
    }

    public List<Submissivel> getListaSubmissiveis() {
        List<Submissivel> list = new ArrayList<>();
        List<Evento> listE = this.le.getListaEventos();
        for (Evento e : listE) {
            if (e.getEstado().isInSubmissao()) {
                list.add(e);
                RegistoSessaoTematica rst = e.getRegistoSessaoTematica();
                ListaSessaoTematica listST = rst.getListaSessoesTematicas();
                List<SessaoTematica> listST2 = listST.getListaSubmissiveis();
                for (SessaoTematica st : listST2) {
                    list.add(st);
                }
            }
        }
        return list;
    }
}
