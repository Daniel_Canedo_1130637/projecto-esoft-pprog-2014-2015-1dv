/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author luis
 */
public class Utilizador {

	private String username;
	private String password;
	private String nome;
	private String email;

	public Utilizador() {
	}

	public Utilizador(String username, String password, String nome,
					  String email) {
		this.username = username;
		this.password = password;
		this.nome = nome;
		this.email = email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean valida() {
		return true;
	}

	public Utilizador clone() {
		Utilizador uClone = new Utilizador();
		uClone.setEmail(email);
		uClone.setNome(nome);
		uClone.setPassword(password);
		uClone.setUsername(username);
		return uClone;
	}

	@Override
	public String toString() {
		return super.toString(); //To change body of generated methods, choose Tools | Templates.
	}

}
