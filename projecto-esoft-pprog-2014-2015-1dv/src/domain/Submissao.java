/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;


public class Submissao {
    
    private Artigo a;
    
    public Submissao()
    {
        
    }
    
    public Artigo novoArtigo()
    {
        return this.a = new Artigo();
    }
    
    public Artigo getArtigo()
    {
        return this.a;
    }
    
    public void setArtigo(Artigo a)
    {
        this.a=a;
    }
}
