/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luis
 */
public class Artigo {

    private String m_strTitulo;
    private String m_strResumo;
    private List<Autor> m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;

    public Artigo() {
    }

    public Artigo(String m_strTitulo, String m_strResumo,
            List<Autor> m_listaAutores, Autor m_autorCorrespondente,
            String m_strFicheiro) {
        this.m_strTitulo = m_strTitulo;
        this.m_strResumo = m_strResumo;
        this.m_listaAutores = m_listaAutores;
        this.m_autorCorrespondente = m_autorCorrespondente;
        this.m_strFicheiro = m_strFicheiro;
    }

    public Artigo(Artigo a) {
        this.m_strTitulo = a.getTitulo();
        this.m_strResumo = a.getResumo();
        this.m_listaAutores = a.getListaAutores();
        this.m_autorCorrespondente = a.getAutorCorrespondente();
        this.m_strFicheiro = a.getFicheiro();
    }

    public String getTitulo() {
        return m_strTitulo;
    }

    public String getResumo() {
        return m_strResumo;
    }

    public List<Autor> getListaAutores() {
        return this.m_listaAutores;
    }

    public Autor getAutorCorrespondente() {
        return this.m_autorCorrespondente;
    }

    public String getFicheiro() {
        return this.m_strFicheiro;
    }

    public void setTitulo(String m_strTitulo) {
        this.m_strTitulo = m_strTitulo;
    }

    public void setResumo(String m_strResumo) {
        this.m_strResumo = m_strResumo;
    }

    public void setFicheiro(String fx) {
        this.m_strFicheiro = fx;
    }

    public void setCorrespondente(Autor autor) {
        this.m_autorCorrespondente = autor;
    }

    public Autor novoAutor(String nome, String afiliacao, String email) {
        Autor aux = new Autor();
        aux.setNome(nome);
        aux.setAfiliacao(afiliacao);
        aux.setEmail(email);
        return aux;
    }

    public boolean addAutor(Autor aut) {
        if (aut.valida()) {
            if (validaAutor(aut)) {
                return this.m_listaAutores.add(aut);
            }
        }
        return false;
    }

    private boolean validaAutor(Autor aut) {
        return true;
    }

    public ArrayList<Autor> getPossiveisAutoresCorrespondentes() {
        ArrayList<Autor> aux = new ArrayList<>();
        return aux;
    }

    public void removerAutor(Autor aut) {
        this.m_listaAutores.remove(aut);
    }

}
