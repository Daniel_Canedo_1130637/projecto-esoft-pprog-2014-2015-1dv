/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class ListaSessaoTematica {

    private List<SessaoTematica> listST;

    public ListaSessaoTematica() {
        listST = new ArrayList<SessaoTematica>();
    }

    public void add(SessaoTematica st) {
        this.listST.add(st);
    }

    public List<SessaoTematica> getCPDefiniveisEmDefinicaoDoUtilizador(String id) {
        List<SessaoTematica> list = new ArrayList<>();
        for (SessaoTematica st : this.listST) {
            if (st.getEstado().isInRegistado()&& st.hasProponente(id)) {
                list.add(st);
            }
        }
        return list;
    }

    List<SessaoTematica> getListaSubmissiveis() {
        List<SessaoTematica> list = new ArrayList<>();
        for (SessaoTematica st : this.listST) {
            if (st.getEstado().isInSubmissao()) {
                list.add(st);
            }
        }
        return list;
    }

}
