/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class ListaEvento {

	List<Evento> le;

	public ListaEvento() {
		le = new ArrayList<>();
	}

	public boolean adicionaEvento(Evento e) {
		return le.add(e);
	}

	public List<Evento> getListaEventoOrganizadorSemCP(String id) {
		return this.le;
	}

	public List<Evento> getListaEventos() {
		return this.le;
	}

	public Evento getNextEventoRegistado() {
		return null;
	}
}
