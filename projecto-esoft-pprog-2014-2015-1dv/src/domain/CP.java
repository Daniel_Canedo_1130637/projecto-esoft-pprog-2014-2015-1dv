/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.List;

/**
 *
 * @author luis
 */
public class CP {

    private List<Revisor> listaRevisores;
    private Revisor r;

    public CP() {
    }

    public CP(List<Revisor> m_listaRevisores) {
        this.listaRevisores = m_listaRevisores;
    }

    public boolean addMembroCP(Utilizador u) {
        this.r = new Revisor();
        if (this.r.valida() || validaRevisor(this.r)) {
            return true;
        }
        return false;
    }
    
    public boolean addMembroCP()
    {
        if(validaRevisor(this.r))
        {
            return addRevisor(this.r);
        }
        
        return false;
    }

    public void setM_listaRevisores(List<Revisor> m_listaRevisores) {
        this.listaRevisores = m_listaRevisores;
    }

    public List<Revisor> getM_listaRevisores() {
        return listaRevisores;
    }

    private boolean validaRevisor(Revisor r) {
        return true;
    }

    private boolean addRevisor(Revisor r) {
        return this.listaRevisores.add(r);
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
