/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author João Cabral
 */
public class Organizador {

	private String id;
	private Utilizador u;

	public Organizador() {
	}

	public Organizador(String id, Utilizador u) {
		this.id = id;
		this.u = u;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Utilizador getU() {
		return u;
	}

	public void setUtilizador(Utilizador u) {
		this.u = u;
	}

	public boolean valida() {
		return true;
	}
}
