/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

public class RegistoUtilizador {

	private ListaUtilizadores lu;

	public RegistoUtilizador() {
		lu = new ListaUtilizadores();
	}

	public Utilizador novoUtilizador() {
		return new Utilizador();
	}

	public boolean registaUtilizador(Utilizador u) {
		if (u.valida() || validaUtilizador(u)) {
			return adicionaUtilizador(u);
		}
		return false;
	}

	private boolean validaUtilizador(Utilizador u) {
		return true;
	}

	private boolean adicionaUtilizador(Utilizador u) {
		return lu.adicionaUtilizador(u);
	}

	public Utilizador getUtilizador(String id) {
		return lu.getUtilizador(id);
	}

	public boolean validaAutorCorrespondente(Autor autor) {
		return true;
	}

	private ListaUtilizadores getListaUtilizadores() {
		return this.lu;
	}
}
