/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EventoState;

/**
 *
 * @author João Cabral
 */
public class EventoRegistadoState implements EventoEstado {

    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public boolean isInCriado() {
        return false;
    }

    @Override
    public boolean isInRegistado() {
        return true;
    }

    @Override
    public boolean isInSTDefinida() {
        return false;
    }

    @Override
    public boolean isInCPDefinida() {
        return false;
    }

    @Override
    public boolean isInSubmissao() {
        return false;
    }

    @Override
    public boolean isInDetecaoConflitos() {
        return false;
    }

    @Override
    public boolean isInLicitacao() {
        return false;
    }

    @Override
    public boolean isInDistribuicao() {
        return false;
    }

    @Override
    public boolean isInRevisao() {
        return false;
    }

    @Override
    public boolean isInDecisao() {
        return false;
    }

    @Override
    public boolean isInDecidido() {
        return false;
    }

    @Override
    public void setRegistado() {
    }

    @Override
    public void setSTDefinida() {
    }

    @Override
    public void setCPDefinida() {
    }

    @Override
    public void setSubmissao() {
    }

    @Override
    public void setDetecaoConflitos() {
    }

    @Override
    public void setLicitacao() {
    }

    @Override
    public void setDistribuicao() {
    }

    @Override
    public void setRevisao() {
    }

    @Override
    public void setDecisao() {
    }

    @Override
    public void setDecidido() {
    }
}
