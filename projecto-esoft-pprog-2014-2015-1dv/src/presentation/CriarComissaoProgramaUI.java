/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import controller.CriarComissaoProgramaController;
import domain.CPDefinivel;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author luis
 */
public class CriarComissaoProgramaUI {

    public Scanner in = new Scanner(System.in);
    
    private CriarComissaoProgramaController controller;

    public CriarComissaoProgramaUI() {
    }

    public CriarComissaoProgramaUI(CriarComissaoProgramaController controller) {
        this.controller = controller;
    }
    
    
    public void run()
    {
        
        System.out.println("Id do utilizador: ");
        String id = in.next();
        
        List<CPDefinivel> list = this.controller.getCPDefinivelEmDefinicaoDoUtilizador(id);
        
        int i=1;
        
        for (CPDefinivel cpDef : list) {
            System.out.println(i+"- "+cpDef.toString());
            i++;
        }
        
        System.out.println("Escolha um");
        int num=in.nextInt();
        
        this.controller.novaCP(list.get(num-1));
        
        
        boolean flag=true;
        
        while(flag)
        {
            System.out.println("Introduza o id do revisor");
            id=in.next();
            
            this.controller.novoMembroCP(id);
            
            System.out.println("Confirma?");
            String c=in.next();
            
            if(c.equals("s")||c.equals("S"))
            {
                this.controller.addMembroCP();
            }
            
            System.out.println("Deseja continuar a adicionar revisores?");
            String op=in.next();
            
            if(op.equals("n")||op.equals("N"))
            {
                flag = false;
            }
        }
        
        this.controller.setCP();
        
        System.out.println("Sucesso da Operação!!");
        
    }
    
}
