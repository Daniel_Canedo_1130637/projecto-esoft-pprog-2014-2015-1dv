/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import controller.AlterarSubmissaoController;
import domain.Autor;
import domain.Empresa;
import domain.Submissao;
import domain.Submissivel;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AlterarSubmissaoUI {

    public Scanner in = new Scanner(System.in);

    private AlterarSubmissaoController asController;

    public AlterarSubmissaoUI() {

    }

    public AlterarSubmissaoUI(Empresa empresa) {
        this.asController = new AlterarSubmissaoController(empresa);
    }

    public void run() {
        String resp;
        boolean flag = true;

        List<Submissivel> l = new ArrayList<>(this.asController.getListaSubmissiveis());

        int i = 1;
        for (Submissivel l1 : l) {
            System.out.println(i + "- " + l1.toString());
            i++;
        }

        int j = in.nextInt();

        Submissivel s = l.get(j - 1);

        ArrayList<Submissao> ls = new ArrayList<>(this.asController.setSubmissivel(s));

        i = 1;

        for (Submissao l1 : ls) {
            System.out.println(i + "- " + l1.toString());
            i++;
        }

        j = in.nextInt();

        Submissao sub = ls.get(j - 1);

        this.asController.setSubmissao(sub);

        System.out.println("Titulo: " + this.asController.getTitulo());
        System.out.println("Resumo: " + this.asController.getResumo());

        System.out.println("Alterar?: (s/n)");

        resp = in.next();

        if (resp.equals("s") || resp.equals("S")) {
            
            String titulo, resumo;

            System.out.println("Introduza o novo titulo: ");
            titulo = in.next();

            System.out.println("Introduza o novo resumo: ");
            resumo = in.next();

            this.asController.alterarDados(titulo, resumo);
        }

        
        System.out.println("Remover Autores?: (s/n)");
        resp = in.next();
        
        if(resp.equals("s") || resp.equals("S"))
        {
            flag = true;
        }
        
        while(flag)
        {
            ArrayList<Autor> la = new ArrayList<>(this.asController.getListaAutores());
            
            i=1;
            
            for (Autor la1 : la) {
                System.out.println(i+"- "+la1.getNome());
            }
            
            j=in.nextInt();
            
            Autor aut = la.get(j-1);
            
            this.asController.setAutor(aut);
            
            System.out.println("Deseja removar mais autores?: (s/n)");
            resp=in.next();
            
            if(resp.equals("n") || resp.equals("N"))
            {
                flag = false;
            }
        }
        
        
        System.out.println("Adicionar Autores?: (s/n)");
        resp = in.next();
        
        if(resp.equals("s") || resp.equals("S"))
        {
            flag = true;
        }
        

        while(flag) {
            String nome, afiliacao, email;

            System.out.println("Introduza nome do autor: ");
            nome = in.next();

            System.out.println("Introduza a afiliacao do autor: ");
            afiliacao = in.next();

            System.out.println("Introduza o email do autor: ");
            email = in.next();

            this.asController.novoAutor(nome, afiliacao, email);

            System.out.println("Confirma? (s/n)");

            resp = in.next();

            if (resp.equals("s") || resp.equals("S")) {
                this.asController.addAutor();
            }

            System.out.println("Continua? (s/n)");

            resp = in.next();

            if (resp.equals("n") || resp.equals("N")) {
                flag = false;
            }

        }

        ArrayList<Autor> lac = new ArrayList<>(this.asController.getPossiveisAutoresCorrespondentes());

        i = 1;

        for (Autor lac1 : lac) {
            System.out.println(i + "- " + lac1.getNome());
            i++;
        }

        j = in.nextInt();

        Autor autor = lac.get(j - 1);

        this.asController.setCorrespondente(autor);

        System.out.println("Insira o ficheiro PDF: ");

        String fx = in.next();

        this.asController.setFicheiro(fx);

        System.out.println("Confirma? (s/n)");

        resp = in.next();

        if (resp.equals("s") || resp.equals("S")) {
            this.asController.alterarArtigo();
        }
        
        
    }
}
