/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import controller.SubmissaoArtigoController;
import domain.Autor;
import domain.Empresa;
import domain.Submissivel;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SubmissaoArtigoUI {

    public Scanner in = new Scanner(System.in);

    private SubmissaoArtigoController saController;

    public SubmissaoArtigoUI() {

    }

    public SubmissaoArtigoUI(Empresa empresa) {
        this.saController = new SubmissaoArtigoController(empresa);
    }

    public void run() {
        List<Submissivel> l = this.saController.getListaSubmissiveis();
        
        int i=1;
        for (Submissivel l1 : l) {
            System.out.println(i+"- "+l1.toString());
            i++;
        }
        
        int j = in.nextInt();
        
        Submissivel s = l.get(j-1);
        
        this.saController.novaSubmissaoEm(s);
        
        String titulo, resumo;
        
        System.out.println("Introduza o titulo: ");
        titulo=in.next();
        
        System.out.println("Introduza o resumo: ");
        resumo=in.next();
        
        this.saController.setDados(titulo, resumo);
        
        boolean flag = true;
        
        String resp;
        
        do
        {
            String nome, afiliacao, email;
            
            System.out.println("Introduza nome do autor: ");
            nome = in.next();
            
            System.out.println("Introduza a afiliacao do autor: ");
            afiliacao = in.next();
            
            System.out.println("Introduza o email do autor: ");
            email = in.next();
            
            this.saController.novoAutor(nome, afiliacao, email);
            
            System.out.println("Confirma? (s/n)");
            
            resp = in.next();
            
            if(resp.equals("s") || resp.equals("S"))
            {
                this.saController.addAutor();
            }
            
            System.out.println("Continua? (s/n)");
            
            resp = in.next();
            
            if(resp.equals("n") || resp.equals("N"))
            {
                flag = false;
            }
            
        }while(flag);
        
        ArrayList<Autor> lac = this.saController.getPossiveisAutoresCorrespondentes();
        
        i=1;
        
        for (Autor lac1 : lac) {
            System.out.println(i+"- "+lac1.getNome());
            i++;
        }
        
        j=in.nextInt();
        
        Autor autor = lac.get(j-1);
        
        this.saController.setCorrespondente(autor);
        
        System.out.println("Insira o ficheiro PDF: ");
        
        String fx = in.next();
        
        this.saController.setFicheiro(fx);
        
        System.out.println("Confirma? (s/n)");
        
        resp=in.next();
        
        if(resp.equals("s") || resp.equals("S"))
        {
            this.saController.registaSubmissao();
        }
        
    }
}
