/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package STState;


public class SessaoDistribuicaoState implements STEstado{
    
    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public boolean isInCriado() {
        return false;
    }

    @Override
    public boolean isInRegistado() {
        return false;
    }

    @Override
    public boolean isInCPDefinida() {
        return false;
    }

    @Override
    public boolean isInSubmissao() {
        return false;
    }

    @Override
    public boolean isInDetecaoConflitos() {
        return false;
    }

    @Override
    public boolean isInLicitacao() {
        return false;
    }

    @Override
    public boolean isInDistribuicao() {
        return true;
    }

    @Override
    public boolean isInRevisao() {
        return false;
    }

    @Override
    public boolean isInDecisao() {
        return false;
    }

    @Override
    public boolean isInDecidido() {
        return false;
    }

    @Override
    public void setRegistado() {
    }

    @Override
    public void setCPDefinida() {
    }

    @Override
    public void setSubmissao() {
    }

    @Override
    public void setDetecaoConflitos() {
    }

    @Override
    public void setLicitacao() {
    }

    @Override
    public void setDistribuicao() {
    }

    @Override
    public void setRevisao() {
    }

    @Override
    public void setDecisao() {
    }

    @Override
    public void setDecidido() {
    }
}
