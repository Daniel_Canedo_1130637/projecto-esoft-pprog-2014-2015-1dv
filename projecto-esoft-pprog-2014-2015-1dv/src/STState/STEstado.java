/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package STState;


public interface STEstado {
    
    public boolean isInCriado();
    
    public boolean isInRegistado();
    
    public boolean isInCPDefinida();
    
    public boolean isInSubmissao();
    
    public boolean isInDetecaoConflitos();
    
    public boolean isInLicitacao();
    
    public boolean isInDistribuicao();
    
    public boolean isInRevisao();
    
    public boolean isInDecisao();
    
    public boolean isInDecidido();
    
    public boolean valida();
    
    public void setRegistado();
    
    public void setCPDefinida();
    
    public void setSubmissao();
    
    public void setDetecaoConflitos();
    
    public void setLicitacao();
    
    public void setDistribuicao();
    
    public void setRevisao();
    
    public void setDecisao();
    
    public void setDecidido();
}
